#include <stream9/xdg/trash/trash_can.hpp>

#include "namespace.hpp"
#include "print_mismatch.hpp"

#include <stream9/xdg/trash/linux_environment.hpp>
#include <stream9/xdg/trash/observer_set.hpp>
#include <stream9/xdg/trash/trash_info.hpp>

#include <concepts>
#include <iomanip>

#include <boost/test/unit_test.hpp>
#include <boost/container/flat_map.hpp>

#include <stream9/array.hpp>
#include <stream9/errors.hpp>
#include <stream9/filesystem/exists.hpp>
#include <stream9/filesystem/is_directory.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/access.hpp>
#include <stream9/linux/chmod.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/dirname.hpp>
#include <stream9/path/is_child_of.hpp>
#include <stream9/path/relative.hpp>
#include <stream9/push_back.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/weak_node.hpp>

namespace testing {

using stream9::shared_node;
using stream9::weak_node;
using stream9::array;
using st9::path::operator/;
using st9::string;
using st9::string_view;

namespace path { using namespace stream9::path; }

static void
add_sticky_bit(string_view path)
{
    auto st = lx::stat(path);
    lx::chmod(path, st.st_mode | S_ISVTX);
}

BOOST_AUTO_TEST_SUITE(trash_can_)

    bool is_writable(string_view p)
    {
        return lx::access(p, W_OK);
    }

    bool has_sticky_bit(string_view p)
    {
        auto st = lx::stat(p);
        return (st.st_mode & S_ISVTX) == S_ISVTX;
    }

    class mock_environment : public trash::linux_environment
    {
    public:
        mock_environment()
        {
            lx::mkdir(m_root / "home");
            create_top_directory(m_root / "top_dir1");
            create_top_directory(m_root / "top_dir2");
        }

        // accessor
        auto& root() const { return m_root; }

        // query
        st9::string home_directory() override { return m_root / "home"; }

        st9::string home_data_directory() override { return m_root / "home_data"; }

        st9::string top_directory(st9::string_view p) override
        {
            using st9::path::is_child_of;
            for (auto& top_dir: m_top_dirs) {
                if (is_child_of(p, top_dir)) {
                    return top_dir;
                }
            }

            return home_directory();
        }

        array<st9::string> mounted_top_directories() override
        {
            return m_top_dirs;
        }

        void add_directory_observer(st9::string_view p,
                                    weak_node<directory_observer> o) override
        {
            m_dir_observers[p].insert(o);
        }

        void add_mount_observer(weak_node<mount_observer> o) override
        {
            m_mount_observers.insert(o);
        }

        // modifier
        void create_top_directory(string_view p)
        {
            lx::mkdir(p);
            st9::push_back(m_top_dirs, p);

            emit_file_created(p);
        }

        string create_file2(string path)
        {
            if (path::is_relative(path)) {
                path = m_root.path() / path;
            }

            st9::ofstream ofs { path };
            BOOST_REQUIRE(ofs.good());
            ofs.close();

            emit_file_created(path);

            return path;
        }

        void emit_file_created(string_view p)
        {
            auto parent = path::dirname(p);

            auto const it = m_dir_observers.find(parent);
            if (it != m_dir_observers.end()) {
                it->second.for_each([&](auto&& o) {
                    o.file_created(it->first, path::basename(p));
                });
            }
        }

        void emit_file_deleted(string_view p)
        {
            auto parent = path::dirname(p);

            auto const it = m_dir_observers.find(parent);
            if (it != m_dir_observers.end()) {
                it->second.for_each([&](auto&& o) {
                    o.file_deleted(it->first, path::basename(p));
                });
            }
        }

        void emit_directory_disappeared(string_view p)
        {
            auto it = m_dir_observers.find(p);
            if (it != m_dir_observers.end()) {
                it->second.for_each([&](auto&& o) {
                    o.directory_disappeared(it->first);
                });
            }
        }

        void emit_filesystem_mounted(string_view p)
        {
            m_mount_observers.for_each([&](auto&& o) {
                o.filesystem_mounted(p);
            });
        }

        void emit_filesystem_unmounted(string_view p)
        {
            m_mount_observers.for_each([&](auto&& o) {
                o.filesystem_unmounted(p);
            });
        }

    private:
        using dir_observer_map =
            boost::container::flat_map<
                string,
                trash::observer_set<environment::directory_observer> >;

        using mount_observer_set =
            trash::observer_set<environment::mount_observer>;

        fs::temporary_directory m_root;
        array<st9::string> m_top_dirs;

        dir_observer_map m_dir_observers;
        mount_observer_set m_mount_observers;
    };

    BOOST_AUTO_TEST_CASE(constract_empty_can_)
    {
        try {
            shared_node<mock_environment> env;

            auto can = trash::trash_can::construct(env);

            BOOST_TEST(can->home_trash_directory().size() == 0);
            BOOST_TEST(can->top_directories().size() == 2);

            auto err = can->check_integrity();
            BOOST_TEST(err.empty());
        }
        catch (...) {
            err::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_SUITE(trash_file_)

        BOOST_AUTO_TEST_CASE(trash_one_on_home_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto p = env->create_file2("foo.txt");

                can->trash_file(p);

                BOOST_TEST(can->home_trash_directory().size() == 1);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(trash_same_name_twice_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto p1 = env->create_file2("foo.txt");
                auto it1 = can->trash_file(p1);

                BOOST_TEST(it1->name() == "foo.txt");

                auto p2 = env->create_file2("foo.txt");
                auto it2 = can->trash_file(p2);

                BOOST_TEST(it2->name() == "foo.txt (1)");

                BOOST_TEST(can->home_trash_directory().size() == 2);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(from_fs_with_writable_top_dir_and_no_trash_dir_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto top_dirs = env->mounted_top_directories();

                // precondition on top directory
                BOOST_REQUIRE(is_writable(top_dirs[0]));
                BOOST_REQUIRE(!fs::is_directory(top_dirs[0] / ".Trash"));
                BOOST_REQUIRE(!fs::is_directory(top_dirs[0] / ".Trash-1000"));

                auto p = top_dirs[0] / "foo.txt";
                env->create_file2(p);

                can->trash_file(p);

                BOOST_TEST(!fs::exists(p));

                auto result = json::value_from(*can);

                json::object expected {
                    { "home_trash",
                        {
                            { "size", 0 },
                            { "path", env->home_data_directory() / "Trash" },
                        },
                    },
                    { "top_dirs", json::array {
                        {
                            { "path", top_dirs[0] },
                            { "trash_dirs", json::array {
                                {
                                    { "path", top_dirs[0] / ".Trash-1000" },
                                    { "size", 1 },
                                }
                            } },
                        },
                    } },
                    { "entries",
                        {
                            { "foo.txt", top_dirs[0] / ".Trash-1000/info/foo.txt.trashinfo" },
                        }
                    },
                };

                BOOST_TEST(json::match(result, expected, print_mismatch));
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(from_fs_with_writable_top_dir_and_trash_dir_without_sticky_bit_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto top_dirs = env->mounted_top_directories();
                lx::mkdir(top_dirs[0] / ".Trash");

                // precondition on top directory
                BOOST_REQUIRE(is_writable(top_dirs[0]));
                BOOST_REQUIRE(fs::is_directory(top_dirs[0] / ".Trash"));
                BOOST_REQUIRE(!has_sticky_bit(top_dirs[0] / ".Trash"));

                auto p = top_dirs[0] / "foo.txt";
                env->create_file2(p);

                can->trash_file(p);

                BOOST_TEST(!fs::exists(p));

                auto result = json::value_from(*can);

                json::object expected {
                    { "home_trash",
                        {
                            { "size", 0 },
                            { "path", env->home_data_directory() / "Trash" },
                        },
                    },
                    { "top_dirs", json::array {
                        {
                            { "path", top_dirs[0] },
                            { "trash_dirs", json::array {
                                {
                                    { "path", top_dirs[0] / ".Trash-1000" },
                                    { "size", 1 },
                                }
                            }},
                        }
                    }},
                };

                BOOST_TEST(json::match(result, expected, print_mismatch));
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(from_fs_with_writable_top_dir_and_top_trash_dir_with_sticky_bit_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto top_dirs = env->mounted_top_directories();
                auto top_trash_dir = top_dirs[0] / ".Trash";

                lx::mkdir(top_trash_dir);
                add_sticky_bit(top_trash_dir);

                // precondition on top directory
                BOOST_REQUIRE(fs::is_directory(top_trash_dir));
                BOOST_REQUIRE(is_writable(top_trash_dir));
                BOOST_REQUIRE(has_sticky_bit(top_trash_dir));

                auto p = top_dirs[0] / "foo.txt";
                env->create_file2(p);

                can->trash_file(p);

                BOOST_TEST(!fs::exists(p));

                auto result = json::value_from(*can);

                json::object expected {
                    { "home_trash",
                        {
                            { "path", env->home_data_directory() / "Trash" },
                            { "size", 0 },
                        },
                    },
                    { "top_dirs", json::array {
                        {
                            { "path", top_dirs[0] },
                            { "trash_dirs", json::array {
                                {
                                    { "path", top_trash_dir / "1000" },
                                    { "size", 1 },
                                },
                            }},
                        },
                    }},
                };

                BOOST_TEST(json::match(result, expected, print_mismatch));
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(from_fs_with_writable_top_dir_and_top_trash_dir_and_uid_trash_dir_)
        {
            try {
                shared_node<mock_environment> env;

                auto top_dirs = env->mounted_top_directories();
                auto top_trash_dir1 = top_dirs[0] / ".Trash";

                lx::mkdir(top_trash_dir1);
                add_sticky_bit(top_trash_dir1);

                auto top_trash_dir2 = top_dirs[0] / ".Trash-1000";
                lx::mkdir(top_trash_dir2);
                lx::mkdir(top_trash_dir2 / "files");
                lx::mkdir(top_trash_dir2 / "info");

                auto can = trash::trash_can::construct(env);

                // precondition on top directory
                BOOST_REQUIRE(fs::is_directory(top_trash_dir1));
                BOOST_REQUIRE(is_writable(top_trash_dir1));
                BOOST_REQUIRE(has_sticky_bit(top_trash_dir1));
                BOOST_REQUIRE(fs::is_directory(top_trash_dir2));
                BOOST_REQUIRE(is_writable(top_trash_dir2));

                auto p = top_dirs[0] / "foo.txt";
                env->create_file2(p);

                json::object before {
                    { "home_trash", { { "size", 0 }, }, },
                    { "top_dirs", json::array {
                        {
                            { "path", top_dirs[0] },
                            { "trash_dirs", json::array {
                                {
                                    { "path", top_trash_dir2 },
                                    { "size", 0 },
                                },
                            }},
                        },
                    }},
                };

                BOOST_REQUIRE(json::match(json::value_from(*can), before, print_mismatch));

                can->trash_file(p);

                BOOST_TEST(!fs::exists(p));

                auto after = json::value_from(*can);

                json::object expected {
                    { "home_trash", { { "size", 0 }, }, },
                    { "top_dirs", json::array {
                        {
                            { "path", top_dirs[0] },
                            { "trash_dirs", json::array {
                                {
                                    { "path", top_trash_dir1 / "1000" },
                                    { "size", 1 },
                                },
                                {
                                    { "path", top_trash_dir2 },
                                    { "size", 0 },
                                },
                            }},
                        },
                    }},
                };

                BOOST_TEST(json::match(after, expected, print_mismatch));
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(file_does_not_exist_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                BOOST_CHECK_EXCEPTION(
                    can->trash_file("XXX"),
                    err::error,
                    [&](auto&& e) {
                        using enum trash::errc;

                        BOOST_TEST(e.why() == file_does_not_exist);

                        return true;
                    });
            }
            catch (...) {
                err::print_error();
                BOOST_TEST(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // trash_file_

    BOOST_AUTO_TEST_CASE(find_1_)
    {
        try {
            shared_node<mock_environment> env;

            auto can = trash::trash_can::construct(env);

            auto p = env->create_file2("foo.txt");
            auto it1 = can->trash_file(p);

            auto it2 = can->find_trash_entry("foo.txt");

            BOOST_TEST((it1 == it2));
        }
        catch (...) {
            err::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(find_2_)
    {
        try {
            shared_node<mock_environment> env;

            auto can = trash::trash_can::construct(env);

            auto p = env->create_file2("foo.txt");
            can->trash_file(p);

            auto it = can->find_trash_entry("bar.txt");

            BOOST_TEST((it == can->end()));
        }
        catch (...) {
            err::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_SUITE(restore_file_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto p = env->create_file2("foo.txt");
                auto it = can->trash_file(p);

                can->restore_file(it);

                BOOST_TEST(can->empty());
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {
            try {
                shared_node<mock_environment> env;

                auto can = trash::trash_can::construct(env);

                auto p = env->create_file2("foo.txt");
                auto it = can->trash_file(p);

                env->create_file2("foo.txt");

                BOOST_CHECK_EXCEPTION(
                    can->restore_file(it),
                    stream9::errors::error,
                    [](auto&& e) {
                        using enum trash::errc;

                        BOOST_TEST(e.why() == file_already_exist);
                        return true;
                    } );

                BOOST_TEST(can->size() == 1);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // restore_file_

    BOOST_AUTO_TEST_SUITE(directory_event_)

        BOOST_AUTO_TEST_CASE(file_created_)
        {
            try {
                shared_node<mock_environment> env;

                auto can1 = trash::trash_can::construct(env);
                auto can2 = trash::trash_can::construct(env);

                auto p = env->create_file2(env->home_directory() / "foo.txt");

                can1->trash_file(p);
                BOOST_TEST(can1->size() == 1);
                BOOST_TEST(can2->size() == 0);

                auto& ent = (*can1)[0];
                env->emit_file_created(ent.trash_info().info_path());

                BOOST_TEST(can1->size() == 1);
                BOOST_TEST(can2->size() == 1);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(file_deleted_)
        {
            try {
                shared_node<mock_environment> env;

                // create first trash_can and trash something in it
                auto can1 = trash::trash_can::construct(env);

                auto p = env->create_file2(env->home_directory() / "foo.txt");
                can1->trash_file(p);

                BOOST_TEST(can1->size() == 1);

                auto it1 = can1->begin();
                string info_path { it1->trash_info().info_path() };
                BOOST_REQUIRE(fs::exists(info_path));

                // create second trash_can and make sure it has already a trash in it
                auto can2 = trash::trash_can::construct(env);
                BOOST_TEST(can2->size() == 1);

                // erase trash from first trash_can
                can1->erase_file(it1);

                BOOST_TEST(can1->size() == 0);
                BOOST_TEST(can2->size() == 1);

                // artificially notify file's deletion from environment
                BOOST_REQUIRE(!fs::exists(info_path));
                env->emit_file_deleted(info_path);

                BOOST_TEST(can1->size() == 0);
                BOOST_TEST(can2->size() == 0);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(directory_disappeared_)
        {
            try {
                shared_node<mock_environment> env;

                // create first trash_can and trash something in it
                auto can1 = trash::trash_can::construct(env);
                auto top_dir1 = env->mounted_top_directories().front();

                auto p = env->create_file2(top_dir1 / "foo.txt");
                can1->trash_file(p);

                BOOST_TEST(can1->size() == 1);
                BOOST_TEST(can1->top_directories().size() == 2);

                // artificially notify directory's disappearance from environment
                env->emit_directory_disappeared(top_dir1);

                BOOST_TEST(can1->size() == 0);
                BOOST_TEST(can1->top_directories().size() == 1);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // directory_event_

    BOOST_AUTO_TEST_SUITE(mount_event_)

        BOOST_AUTO_TEST_CASE(filesystem_mounted_)
        {
            try {
                shared_node<mock_environment> env;

                auto can1 = trash::trash_can::construct(env);

                BOOST_TEST(can1->top_directories().size() == 2);

                // create new top directory as fake mountpoint
                auto top_dir_X = env->root() / "top_dir_X";
                env->create_top_directory(top_dir_X);

                // artificially notify filesystem mount from environment
                env->emit_filesystem_mounted(top_dir_X);

                BOOST_TEST(can1->top_directories().size() == 3);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

        BOOST_AUTO_TEST_CASE(filesystem_unmounted_)
        {
            try {
                shared_node<mock_environment> env;

                // create trash can and trash a file in it
                auto can1 = trash::trash_can::construct(env);

                auto top_dir1 = env->mounted_top_directories().front();
                auto p1 = env->create_file2(top_dir1 / "foo");

                can1->trash_file(p1);

                BOOST_TEST(can1->top_directories().size() == 2);
                BOOST_TEST(can1->size() == 1);

                // artificially notify filesystem unmount from environment
                env->emit_filesystem_unmounted(top_dir1);

                BOOST_TEST(can1->top_directories().size() == 1);
                BOOST_TEST(can1->size() == 0);

                // artificially notify filesystem mount from environment
                env->emit_filesystem_mounted(top_dir1);

                BOOST_TEST(can1->top_directories().size() == 2);
                BOOST_TEST(can1->size() == 1);
            }
            catch (...) {
                err::print_error();
                BOOST_REQUIRE(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // mount_event_

    static void
    save_file(st9::string_view path, st9::string_view contents)
    {
        std::ofstream ofs { st9::string(path) };
        ofs << contents;
    }

    BOOST_AUTO_TEST_CASE(bug_fix_1_)
    {
        try {
            auto can = trash::trash_can::construct();

            //std::cout << std::setw(2) << json::value_from(*can) << std::endl;

            save_file("/tmp/xxx", "foo");

            can->trash_file("/tmp/xxx");
        }
        catch (...) {
            st9::print_error();
            BOOST_TEST(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // trash_can_

} // namespace testing
