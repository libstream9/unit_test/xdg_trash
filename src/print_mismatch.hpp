#ifndef STREAM9_TRASH_TEST_SRC_PRINT_MISMATCH_HPP
#define STREAM9_TRASH_TEST_SRC_PRINT_MISMATCH_HPP

#include <iostream>

#include <stream9/json/algorithm.hpp>

namespace testing {

inline bool
print_mismatch(json::pointer const& p,
               json::value const& v1, json::value const& v2)
{
    std::cout << "mismatch at " << p
              << ", result = " << v1
              << ", expected = " << v2 << std::endl;
    return false;
};

} // namespace testing

#endif // STREAM9_TRASH_TEST_SRC_PRINT_MISMATCH_HPP
