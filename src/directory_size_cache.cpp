#include <stream9/xdg/trash/directory_size_cache.hpp>

#include "namespace.hpp"
#include "print_mismatch.hpp"

#include <stream9/xdg/trash/linux_environment.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>

#include <filesystem>

#include <boost/test/unit_test.hpp>

#include <stream9/cstring_ptr.hpp>
#include <stream9/errors.hpp>
#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/filesystem/size_recursive.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/weak_node.hpp>

namespace testing {

using st9::cstring_ptr;
using st9::opt;
using st9::shared_node;
using st9::string;
using st9::string_view;
using st9::weak_node;

namespace path = st9::path;
using path::operator/;

BOOST_AUTO_TEST_SUITE(directory_size_cache_)

    class mock_environment : public trash::linux_environment
    {
    public:
        string_view root() const { return m_home.path(); }

        void emit_file_created(string_view p)
        {
            if (auto ob = m_observer->lock()) {
                ob->file_created(p, path::basename(p));
            }
        }

        void emit_file_modified(string_view p)
        {
            if (auto ob = m_observer->lock()) {
                ob->file_modified(p, path::basename(p));
            }
        }

        void emit_file_deleted(string_view p)
        {
            if (auto ob = m_observer->lock()) {
                ob->file_deleted(p, path::basename(p));
            }
        }

        // trash::environment
        void add_directory_observer(string_view,
                                    weak_node<directory_observer> p) override
        {
            m_observer = p;
        }

        trash::file_size_t
        file_or_directory_size(cstring_ptr const& path) override
        {
            return fs::size_recursive(path);
        }

    private:
        fs::temporary_directory m_home;
        opt<weak_node<directory_observer>> m_observer;
    };

    static mock_environment&
    install_mock_environment()
    {
        shared_node<mock_environment> env;
        trash::set_env(env);
        return env;
    }

    static string make_directory(string_view p)
    {
        fs::mkdir_recursive(p);

        st9::ofstream ofs { p / "foo.txt" };
        ofs << "foo";

        return p;
    }

    static string create_fake_cache(string_view path)
    {
        using namespace std::literals;

        auto p = path / "directorysizes";
        st9::ofstream ofs { p };

        ofs << "3000 1000 foo\n"
            << "2000 1000 bar%24\n"
            ;

        ofs.close();

        using std::filesystem::last_write_time;
        last_write_time(p.c_str(), std::chrono::file_clock::now() + 3s);

        return p;
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(c->empty());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(load_cache_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            create_fake_cache(trash_dir);

            auto c = trash::directory_size_cache::construct(*d);

            json::array expected1 {
                { { "filename", "bar$" }, { "size", 2000 }, },
                { { "filename", "foo" }, { "size", 3000 }, },
            };

            BOOST_TEST(json::match(json::value_from(*c), expected1, print_mismatch));

            auto cache = fs::load_string(c->path());

            auto expected2 = "3000 1000 foo\n"
                             "2000 1000 bar%24\n";

            BOOST_TEST(cache == expected2);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(create_cache_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto p1 = make_directory(env.root() / "foo");
            d->trash_file(p1.c_str());

            auto p2 = make_directory(env.root() / "bar$");
            d->trash_file(p2.c_str());

            BOOST_TEST(d->size() == 2);

            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(c->size() == 2);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(find_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            create_fake_cache(trash_dir);

            auto c = trash::directory_size_cache::construct(*d);

            auto it = c->find("foo");
            BOOST_REQUIRE((it != c->end()));

            auto result = json::value_from(*it);

            json::object expected {
                { "filename", "foo" },
                { "size", 3000 },
                { "mtime", "1970-01-01 09:00:01" },
            };

            BOOST_TEST(result == expected);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(insert_entry_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto p1 = make_directory(env.root() / "foo");
            d->trash_file(p1.c_str());

            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(d->size() == 1);

            auto p2 = make_directory(env.root() / "bar$");
            auto& info = d->trash_file(p2.c_str());

            c->insert_or_update_entry(info);

            BOOST_TEST(c->size() == 2);

            //std::cout << json::value_from(c) << std::endl;
            //std::cout << fs::load_string(c.path()) << std::endl;
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(update_entry_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto p1 = make_directory(env.root() / "foo");
            auto& info = d->trash_file(p1.c_str());

            auto c = trash::directory_size_cache::construct(*d);
            BOOST_TEST(c->size() == 1);
            BOOST_TEST(c->front().size >= 3);

            // change size of trashed directory by creating a new file
            std::ofstream ofs { info.file_path() / "bar" };
            ofs << "baaaaaaaaaaaaaaaaaaaaaaaaar"; // length: 27
            ofs.close();

            c->update_or_insert_entry(info);

            BOOST_TEST(c->size() == 1);
            BOOST_TEST(c->front().size >= 3 + 27);

            //std::cout << json::value_from(c) << std::endl;
            //std::cout << fs::load_string(c.path()) << std::endl;
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(erase_entry_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto p1 = make_directory(env.root() / "foo");
            auto& info = d->trash_file(p1.c_str());

            auto c = trash::directory_size_cache::construct(*d);
            BOOST_TEST(c->size() == 1);

            c->erase_entry(info);

            BOOST_TEST(c->size() == 0);

            //std::cout << json::value_from(c) << std::endl;
            //std::cout << fs::load_string(c.path()) << std::endl;
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(refresh_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto p1 = make_directory(env.root() / "foo");
            d->trash_file(p1.c_str());

            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(c->size() == 1);

            create_fake_cache(trash_dir);

            c->refresh();

            BOOST_TEST(c->size() == 2);
#if 0
            std::cout << json::value_from(c) << std::endl;
            std::cout << fs::load_string(c.path()) << std::endl;
#endif
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(file_created_event_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(c->size() == 0);

            auto p1 = create_fake_cache(trash_dir);
            env.emit_file_created(p1);

            BOOST_TEST(c->size() == 2);
#if 0
            std::cout << json::value_from(c) << std::endl;
            std::cout << fs::load_string(c.path()) << std::endl;
#endif
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(file_modified_event_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(c->size() == 0);

            auto p1 = create_fake_cache(trash_dir);
            env.emit_file_modified(p1);

            BOOST_TEST(c->size() == 2);
#if 0
            std::cout << json::value_from(c) << std::endl;
            std::cout << fs::load_string(c.path()) << std::endl;
#endif
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(file_deleted_event_)
    {
        try {
            auto& env = install_mock_environment();

            auto trash_dir = env.root() / "Trash";
            auto d = trash::trash_directory::construct(
                               trash_dir.c_str(), { .create_if_not_exist = true });

            auto p1 = create_fake_cache(trash_dir);
            auto c = trash::directory_size_cache::construct(*d);

            BOOST_TEST(c->size() == 2);

            lx::unlink(p1);
            env.emit_file_deleted(p1);

            BOOST_TEST(c->size() == 0);
#if 0
            std::cout << json::value_from(c) << std::endl;
            std::cout << fs::load_string(c.path()) << std::endl;
#endif
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // directory_size_cache_

} // namespace testing
