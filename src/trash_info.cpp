#include <stream9/xdg/trash/trash_info.hpp>

#include "namespace.hpp"

#include <stream9/filesystem/exists.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/to_string.hpp>

#include <concepts>
#include <filesystem>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::path::operator/;

BOOST_AUTO_TEST_SUITE(trash_info_)

    static auto
    last_write_time(auto&& p)
    {
        using std::chrono::file_clock;
        using std::filesystem::last_write_time;

        return file_clock::to_sys(last_write_time(p.c_str()));
    }

    BOOST_AUTO_TEST_CASE(construct_from_trashinfo_file_)
    {
        try {
            fs::temporary_directory trash_dir;
            lx::mkdir(trash_dir.path() / "info");
            lx::mkdir(trash_dir.path() / "files");
            auto trash_info_path = trash_dir.path() / "info/bar.txt.trashinfo";

            st9::ofstream ofs { trash_info_path };

            ofs << "[Trash Info]\n"
                << "Path=/home/stream/bar.txt\n"
                << "DeletionDate=2021-01-26T19:32:58"
                << std::flush;

            auto trash_file_path = trash_dir.path() / "files/bar.txt";
            st9::ofstream ofs1 { trash_file_path };
            ofs1 << "bar" << std::flush;
            BOOST_REQUIRE(fs::exists(trash_file_path));;

            trash::trash_info i { trash_info_path };

            json::object expected {
                { "type", "stream9::trash::trash_info" },
                { "name", "bar.txt" },
                { "info_path", trash_info_path },
                { "file_path", trash_file_path },
                { "original_path", "/home/stream/bar.txt" },
                { "deletion_date", "2021-01-26 19:32:58" },
                { "time_stamp", str::to_string(last_write_time(trash_info_path)) },
            };

            json::patch p { json::value_from(i), expected };
            BOOST_TEST(p.empty(), p);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(construct_by_creating_trashinfo_file_)
    {
        fs::temporary_directory trash_dir;
        auto trash_info_dir = trash_dir.path() / "info";
        lx::mkdir(trash_info_dir);

        trash::trash_info i {
            trash_info_dir,
            "/home/stream/bar.txt"
        };

        auto trash_info_path = trash_info_dir / "bar.txt.trashinfo";
        BOOST_TEST_REQUIRE(fs::exists(trash_info_path));

        using std::chrono::time_point_cast;
        using std::chrono::system_clock;
        using std::chrono::seconds;
        auto const deletion_date = time_point_cast<seconds>(system_clock::now());
        auto const time_stamp = last_write_time(trash_info_path);

        json::object expected {
            { "type", "stream9::trash::trash_info" },
            { "name", "bar.txt" },
            { "info_path", trash_info_path },
            { "file_path", trash_dir / "files/bar.txt" },
            { "original_path", "/home/stream/bar.txt" },
            { "deletion_date", str::to_string(deletion_date) },
            { "time_stamp", str::to_string(time_stamp) },
        };

        json::patch p { json::value_from(i), expected };
        BOOST_TEST(p.empty(), p);
    }

    BOOST_AUTO_TEST_CASE(create_trash_which_have_same_name_twise_)
    {
        try {
            fs::temporary_directory trash_dir;
            auto trash_info_dir = trash_dir.path() / "info";
            lx::mkdir(trash_info_dir);

            trash::trash_info i1 {
                trash_info_dir,
                "/home/stream/bar.txt"
            };

            BOOST_TEST(i1.info_path() == trash_info_dir / "bar.txt.trashinfo");

            trash::trash_info i2 {
                trash_info_dir,
                "/home/stream/bar.txt"
            };

            BOOST_TEST(i2.info_path() == trash_info_dir / "bar.txt (1).trashinfo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(original_path_that_ends_with_slash_)
    {
        try {
            fs::temporary_directory trash_dir;
            auto trash_info_dir = trash_dir.path() / "info";
            lx::mkdir(trash_info_dir);

            trash::trash_info i1 {
                trash_info_dir,
                "/foo/"
            };

            BOOST_TEST(i1.info_path() == trash_info_dir / "foo.trashinfo");
            BOOST_TEST(i1.original_path() == "/foo");
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(query_)
    {
        fs::temporary_directory trash_dir;
        lx::mkdir(trash_dir.path() / "info");
        lx::mkdir(trash_dir.path() / "files");
        auto trash_info_path = trash_dir.path() / "info/bar.txt.trashinfo";
        auto trash_file_path = trash_dir.path() / "files/bar.txt";

        st9::ofstream ofs { trash_info_path };

        ofs << "[Trash Info]\n"
            << "Path=/home/stream/bar.txt\n"
            << "DeletionDate=2021-01-26T19:32:58"
            << std::flush;
        BOOST_TEST(ofs.good());

        st9::ofstream ofs2 { trash_file_path };
        ofs2 << "bar" << std::flush;
        BOOST_REQUIRE(fs::exists(trash_file_path));

        trash::trash_info i { trash_info_path };

        BOOST_TEST(i.name() == "bar.txt");
        BOOST_TEST(i.info_path() == trash_info_path);
        BOOST_TEST(i.file_path() == trash_dir.path() / "files/bar.txt");
        BOOST_TEST(i.original_path() == "/home/stream/bar.txt");
        BOOST_TEST(str::to_string(i.deletion_date()) == "2021-01-26 19:32:58");

        BOOST_TEST((i.time_stamp() == last_write_time(trash_info_path)));
    }

    BOOST_AUTO_TEST_CASE(reload_)
    {
        fs::temporary_directory trash_dir;
        lx::mkdir(trash_dir.path() / "info");
        lx::mkdir(trash_dir.path() / "files");
        auto trash_info_path = trash_dir.path() / "info/bar.txt.trashinfo";
        auto trash_file_path = trash_dir.path() / "files/bar.txt";

        st9::ofstream ofs { trash_info_path };

        ofs << "[Trash Info]\n"
            << "Path=/home/stream/bar.txt\n"
            << "DeletionDate=2021-01-26T19:32:58"
            << std::flush;
        BOOST_TEST(ofs.good());

        st9::ofstream ofs2 { trash_file_path };
        ofs2 << "bar" << std::flush;
        BOOST_REQUIRE(fs::exists(trash_file_path));

        trash::trash_info i { trash_info_path };

        ofs.close();
        ofs.open(trash_info_path, ofs.out | ofs.trunc);
        ofs << "[Trash Info]\n"
            << "Path=/home/stream/foo.txt\n"
            << "DeletionDate=2021-01-26T19:32:58"
            << std::flush;
        BOOST_TEST(ofs.good());

        i.reload();
        BOOST_TEST(i.original_path() == "/home/stream/foo.txt");
    }

    //TODO test errors

BOOST_AUTO_TEST_SUITE_END() // trash_info_

} // namespace testing
