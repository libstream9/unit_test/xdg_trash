#include <stream9/xdg/trash/top_directory_set.hpp>

#include "namespace.hpp"
#include "print_mismatch.hpp"

#include <stream9/xdg/trash/linux_environment.hpp>
#include <stream9/xdg/trash/top_directory.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/filesystem/remove_recursive.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/relative.hpp>
#include <stream9/push_back.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/weak_node.hpp>

namespace testing {

using stream9::array;
using stream9::shared_node;
using stream9::string;
using stream9::string_view;
using stream9::weak_node;

namespace path = stream9::path;
using path::operator/;

BOOST_AUTO_TEST_SUITE(top_directory_set_)

    class mock_environment : public trash::linux_environment
    {
    public:
        mock_environment()
        {}

        // accessor
        auto& root() const { return m_root; }

        // modifier
        string create_top_directory(string p)
        {
            if (path::is_relative(p)) {
                p = m_root.path() / p;
            }

            lx::mkdir(p);
            st9::push_back(m_top_dirs, p);

            m_observers.for_each([&](auto&& o) {
                o.filesystem_mounted(p);
            });

            return p;
        }

        void delete_top_directory(string_view p)
        {
            fs::remove_recursive(p);

            st9::erase_if(m_top_dirs, [&](auto&& p1) { return p1 == p; });

            m_observers.for_each([&](auto&& o) {
                o.filesystem_unmounted(p);
            });
        }

        array<st9::string> mounted_top_directories() override
        {
            return m_top_dirs;
        }

        st9::string top_directory(st9::string_view p) override
        {
            return st9::string(p);
        }

        void add_mount_observer(weak_node<mount_observer> o) override
        {
            m_observers.insert(o);
        }

    private:
        fs::temporary_directory m_root;
        array<st9::string> m_top_dirs;
        trash::observer_set<environment::mount_observer> m_observers;
    };

    static mock_environment&
    install_mock_environment()
    {
        shared_node<mock_environment> env;
        trash::set_env(env);
        return env;
    }

    class observer : public trash::top_directory_set::observer
    {
    public:
        auto& events() const noexcept { return m_events; }

        void top_directory_added(trash::top_directory& d) override
        {
            m_events.push_back(json::object {
                { "type", "top_directory_added" },
                { "path", d.path() },
            });
        }

        void top_directory_about_to_be_removed(trash::top_directory& d) override
        {
            m_events.push_back(json::object { //TODO push_back(value_ref)
                { "type", "top_directory_about_to_be_removed" },
                { "path", d.path() },
            });
        }

    private:
        json::array m_events;
    };

#if 0
    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(empty_)
        {
            install_mock_environment();

            auto set = trash::top_directory_set::construct();

            BOOST_TEST(set->empty());
        }

        BOOST_AUTO_TEST_CASE(one_)
        {
            auto& env = install_mock_environment();

            auto p1 = env.create_top_directory("foo");

            auto set = trash::top_directory_set::construct();

            BOOST_TEST(set->size() == 1);

            auto result = json::value_from(*set);

            json::array expected {
                { { "path", p1 } },
            };

            BOOST_TEST(json::match(result, expected, print_mismatch));
        }

        BOOST_AUTO_TEST_CASE(two_)
        {
            auto& env = install_mock_environment();

            auto p1 = env.create_top_directory("dir1");
            auto p2 = env.create_top_directory("dir2");

            auto set = trash::top_directory_set::construct();

            BOOST_TEST(set->size() == 2);

            auto result = json::value_from(*set);

            json::array expected {
                { { "path", p1 } },
                { { "path", p2 } },
            };

            BOOST_TEST(json::match(result, expected, print_mismatch));
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(find_)

        BOOST_AUTO_TEST_CASE(fail_1_)
        {
            install_mock_environment();

            auto set = trash::top_directory_set::construct();

            auto it = set->find("foo");

            BOOST_CHECK(it == set->end());
        }

        BOOST_AUTO_TEST_CASE(fail_2_)
        {
            auto& env = install_mock_environment();
            auto p1 = env.create_top_directory("dir1");

            auto set = trash::top_directory_set::construct();

            auto it = set->find(env.root() / "dir2");

            BOOST_REQUIRE(it == set->end());
        }

        BOOST_AUTO_TEST_CASE(success_1_)
        {
            auto& env = install_mock_environment();
            auto p1 = env.create_top_directory("dir1");

            auto set = trash::top_directory_set::construct();

            auto it = set->find(env.root() / "dir1");

            BOOST_REQUIRE(it != set->end());
            BOOST_TEST(it->path() == p1);
        }

    BOOST_AUTO_TEST_SUITE_END() // find_
#endif

    BOOST_AUTO_TEST_SUITE(mount_event_)

        BOOST_AUTO_TEST_CASE(mounted_)
        {
            auto& env = install_mock_environment();

            auto set = trash::top_directory_set::construct();

            shared_node<observer> ob;
            set->add_observer(ob);

            auto p1 = env.create_top_directory("dir1");

            auto& result = ob->events();

            json::array expected { {
                { "type", "top_directory_added" },
                { "path", p1 },
            } };

            BOOST_TEST(result == expected);
        }

        BOOST_AUTO_TEST_CASE(unmounted_)
        {
            auto& env = install_mock_environment();

            auto p1 = env.create_top_directory("dir1");

            auto set = trash::top_directory_set::construct();

            shared_node<observer> ob;
            set->add_observer(ob);

            env.delete_top_directory(p1);

            auto& result = ob->events();

            json::array expected { {
                { "type", "top_directory_about_to_be_removed" },
                { "path", p1 },
            } };

            BOOST_TEST(result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // mount_event_

BOOST_AUTO_TEST_SUITE_END() // top_directory_set_

} // namespace testing
