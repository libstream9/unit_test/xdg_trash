#include <stream9/xdg/trash/trash_directory.hpp>

#include "data_dir.hpp"
#include "namespace.hpp"
#include "print_mismatch.hpp"

#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/linux_environment.hpp>

#include <chrono>
#include <concepts>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/filesystem/exists.hpp>
#include <stream9/filesystem/size_recursive.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/test/scoped_cwd.hpp>

namespace testing {

using namespace std::literals;
using namespace json::literals;
using st9::shared_node;
using st9::weak_node;
using st9::path::operator/;
using st9::string_view;

BOOST_AUTO_TEST_SUITE(directory_)

    class mock_environment : public trash::linux_environment
    {
    public:
        using file_size_t = trash::file_size_t;

    public:
        void add_directory_observer(string_view,
                                    weak_node<directory_observer> ob) override
        {
            m_observers.push_back(ob);
        }

        file_size_t file_or_directory_size(str::cstring_ptr const& p) override
        {
            return fs::size_recursive(p);
        }

        file_size_t filesystem_capacity(string_view p) override
        {
            if (m_capacity) {
                return *m_capacity;
            }
            else {
                return linux_environment::filesystem_capacity(p);
            }
        }

        void emit_file_created(string_view dir, string_view filename)
        {
            for (auto p: m_observers) {
                if (auto o = p.lock()) {
                    o->file_created(dir, filename);
                }
            }
        }

        void emit_file_modified(string_view dir, string_view filename)
        {
            for (auto p: m_observers) {
                if (auto o = p.lock()) {
                    o->file_modified(dir, filename);
                }
            }
        }

        void emit_file_deleted(string_view dir, string_view filename)
        {
            for (auto p: m_observers) {
                if (auto o = p.lock()) {
                    o->file_deleted(dir, filename);
                }
            }
        }

        void emit_directory_disappeared(string_view dir)
        {
            for (auto p: m_observers) {
                if (auto o = p.lock()) {
                    o->directory_disappeared(dir);
                }
            }
        }

        void set_filesystem_capacity(file_size_t s)
        {
            m_capacity = s;
        }

    private:
        std::vector<weak_node<directory_observer>> m_observers;
        std::optional<file_size_t> m_capacity;
    };

    mock_environment&
    install_mock_environment()
    {
        shared_node<mock_environment> env;
        trash::set_env(env);
        return env;
    }

    struct observer : public trash::trash_directory::observer
    {
        json::array events;

        void trash_info_created(trash::trash_directory& d, trash::trash_info& i) override
        {
            events.push_back(json::object {
                { "event", "entry_created" },
                { "directory", d.path() },
                { "entry", i.info_path() },
            });
        }

        void trash_info_deleted(trash::trash_directory& d, trash::trash_info const& i) override
        {
            events.push_back(json::object {
                { "event", "entry_deleted" },
                { "directory", d.path() },
                { "entry", i.info_path() },
            });
        }

        void trash_info_modified(trash::trash_directory& d, trash::trash_info& i) override
        {
            events.push_back(json::object {
                { "event", "entry_modified" },
                { "directory", d.path() },
                { "entry", i.info_path() },
            });
        }

        void directory_disappeared(trash::trash_directory& d) override
        {
            events.push_back(json::object {
                { "event", "directory_disappeared" },
                { "directory", d.path() },
            });
        }
    };

    BOOST_AUTO_TEST_CASE(basic_)
    {
        install_mock_environment();
        auto d = trash::trash_directory::construct((data_dir() / "Trash").c_str());

        BOOST_TEST_REQUIRE(d->size() == 2);
        BOOST_TEST((*d)[0].name() == "bar.txt");
        BOOST_TEST((*d)[1].name() == "foo");
    }

    void create_directory_tree(string_view dir, json::value const& tree) //TODO extract into stream9::filesystem
    {
        if (!tree.is_object()) return; //TODO throw error

        for (auto&& [name, value]: tree.get_object()) {
            if (value.is_object()) {
                auto subdir = dir / name;

                lx::mkdir(subdir);
                create_directory_tree(subdir, value.get_object());
            }
            else if (value.is_array()) {
                std::ofstream ofs { dir / name };
                for (auto const& line: value.get_array()) {
                    ofs << line << std::endl;
                }
            }
            else if (value.is_string()) { // prevent double quoting by JSON serializer
                std::ofstream ofs { dir / name };
                ofs << string_view(value.get_string());
            }
            else {
                std::ofstream ofs { dir / name };
                ofs << value;
            }
        }
    }

    BOOST_AUTO_TEST_SUITE(file_size_)

        BOOST_AUTO_TEST_CASE(file_with_iterator_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path(), { .create_if_not_exist = true, });

                fs::temporary_directory file_dir;

                create_directory_tree(file_dir.path(), json::object {
                    { "file1", "foo" },
                });

                d->trash_file(file_dir / "file1");

                auto result = d->file_size(d->begin());

                BOOST_TEST(result == 3);
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(directory_with_iterator_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path(), { .create_if_not_exist = true, });

                fs::temporary_directory file_dir;

                create_directory_tree(file_dir.path(), json::object {
                    { "dir1", json::object { { "file2", "0123456789" } } },
                });

                d->trash_file(file_dir / "dir1");

                auto result = d->file_size(d->begin());

                BOOST_TEST(result >= 10);
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(with_trash_info_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path().c_str(), { .create_if_not_exist = true, });

                fs::temporary_directory file_dir;

                create_directory_tree(file_dir.path(), json::object {
                    { "file1", "foo" },
                });

                auto& info = d->trash_file(file_dir / "file1");

                auto result = d->file_size(info);

                BOOST_TEST(result == 3);
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // file_size_

    BOOST_AUTO_TEST_SUITE(directory_size_)

        BOOST_AUTO_TEST_CASE(empty_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path().c_str(), { .create_if_not_exist = true, });

                BOOST_TEST(d->directory_size() == 0);
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // directory_size_

    BOOST_AUTO_TEST_SUITE(trash_file_)

        BOOST_AUTO_TEST_CASE(with_absolute_path_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path().c_str(), { .create_if_not_exist = true, });

                shared_node<observer> ob;
                d->add_observer(ob);

                fs::temporary_directory file_dir;
                std::ofstream ofs { file_dir / "foo" };

                BOOST_TEST(d->empty());

                d->trash_file(file_dir / "foo");

                BOOST_REQUIRE(d->size() == 1);
                BOOST_TEST(fs::exists(trash_dir / "info" / "foo.trashinfo"));
                BOOST_TEST(!fs::exists(file_dir / "foo"));

                auto& info = d->front();
                auto result = json::value_from(info); //TODO generic tag_invoke for range

                json::erase(result, "/deletion_date"_jp);
                json::erase(result, "/time_stamp"_jp);

                auto expected = json::object {
                    { "type", "stream9::trash::trash_info" },
                    { "name", "foo" },
                    { "info_path", trash_dir / "info" / "foo.trashinfo" },
                    { "file_path", trash_dir / "files" / "foo" },
                    { "original_path", file_dir / "foo" },
                };

                BOOST_TEST(result == expected); //TODO json::include

                auto expected_events = json::array {
                    { { "event", "entry_created" },
                      { "directory", trash_dir.path() },
                      { "entry", info.info_path() },
                    }
                };
                BOOST_TEST(ob->events == expected_events);
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(with_relative_path_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path(), { .create_if_not_exist = true, });

                fs::temporary_directory file_dir;
                std::ofstream ofs { file_dir / "foo" };

                st9::test::scoped_cwd cwd { file_dir.path() };

                auto& info = d->trash_file("foo");

                BOOST_TEST(info.original_path() == file_dir / "foo");
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_SUITE(size_limit_)

            BOOST_AUTO_TEST_CASE(trash_is_too_big_)
            {
                try {
                    fs::temporary_directory trash_dir;

                    auto& env = install_mock_environment();
                    // default size limit is 0.01 of fs capacity therefore
                    // directory size limit will be 10 bytes.
                    env.set_filesystem_capacity(10 * 100);

                    auto d = trash::trash_directory::construct(
                        trash_dir.path().c_str(), { .create_if_not_exist = true, });

                    fs::temporary_directory file_dir;
                    std::ofstream ofs { file_dir / "foo" };
                    ofs << "12345678901";
                    ofs.close();

                    BOOST_CHECK_EXCEPTION(
                        d->trash_file(file_dir / "foo"),
                        stream9::errors::error,
                        [&](auto&& e) {
                            BOOST_TEST(e.why() == trash::errc::file_is_too_big);
                            return true;
                        });
                }
                catch (...) {
                    stream9::errors::print_error();
                    BOOST_TEST(false);
                }
            }

            BOOST_AUTO_TEST_CASE(auto_deletion_)
            {
                try {
                    fs::temporary_directory trash_dir;

                    auto& env = install_mock_environment();
                    // default size limit is 0.01 of fs capacity therefore
                    // directory size limit will be 10 bytes.
                    env.set_filesystem_capacity(10 * 100);

                    auto d = trash::trash_directory::construct(
                        trash_dir.path().c_str(), { .create_if_not_exist = true, });

                    fs::temporary_directory file_dir;
                    std::ofstream ofs { file_dir / "foo" };
                    ofs << "123456789";
                    ofs.close();

                    d->trash_file(file_dir / "foo");

                    BOOST_TEST(d->directory_size() == 9);
                    BOOST_TEST(d->size() == 1);

                    ofs.open(file_dir / "bar");
                    ofs << "12345";
                    ofs.close();

                    d->trash_file(file_dir / "bar");

                    BOOST_TEST(d->directory_size() == 5);
                    BOOST_TEST(d->size() == 1);
                    BOOST_TEST(d->front().name() == "bar");
                }
                catch (...) {
                    stream9::errors::print_error();
                    BOOST_TEST(false);
                }
            }

        BOOST_AUTO_TEST_SUITE_END() // size_limit_

    BOOST_AUTO_TEST_SUITE_END() // trash_file_

    BOOST_AUTO_TEST_SUITE(restore_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path().c_str(), { .create_if_not_exist = true, });

                shared_node<observer> ob;
                d->add_observer(ob);

                fs::temporary_directory file_dir;
                std::ofstream ofs { file_dir / "foo" };

                BOOST_TEST(d->empty());

                d->trash_file(file_dir / "foo");

                BOOST_REQUIRE(d->size() == 1);
                BOOST_TEST(fs::exists(trash_dir / "info" / "foo.trashinfo"));
                BOOST_TEST(!fs::exists(file_dir / "foo"));

                ob->events.clear();

                d->restore_trash(d->begin());

                BOOST_TEST(d->empty());
                BOOST_TEST(!fs::exists(trash_dir / "info" / "foo.trashinfo"));
                BOOST_TEST(fs::exists(file_dir / "foo"));

                auto expected_events = json::array {
                    { { "event", "entry_deleted", },
                      { "directory", trash_dir.path() },
                    }
                };
                BOOST_TEST(json::match(ob->events, expected_events, print_mismatch));
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {
            try {
                fs::temporary_directory trash_dir;

                install_mock_environment();
                auto d = trash::trash_directory::construct(
                    trash_dir.path().c_str(), { .create_if_not_exist = true, });

                shared_node<observer> ob;
                d->add_observer(ob);

                fs::temporary_directory file_dir;
                std::ofstream ofs1 { file_dir / "foo" };

                BOOST_TEST(d->empty());

                d->trash_file(file_dir / "foo");

                BOOST_REQUIRE(d->size() == 1);
                BOOST_TEST(fs::exists(trash_dir / "info" / "foo.trashinfo"));
                BOOST_TEST(!fs::exists(file_dir / "foo"));

                std::ofstream ofs2 { file_dir / "foo" };

                ob->events.clear();

                BOOST_CHECK_EXCEPTION(
                    d->restore_trash(d->begin()),
                    stream9::errors::error,
                    [](auto&& e) {
                        using E = trash::errc;
                        BOOST_TEST(e.why() == E::file_already_exist);
                        return true;
                    } );

                BOOST_TEST(ob->events.empty());
            }
            catch (...) {
                stream9::errors::print_error();
                BOOST_TEST(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // restore_

    BOOST_AUTO_TEST_CASE(delete_trash_)
    {
        try {
            fs::temporary_directory trash_dir;

            install_mock_environment();
            auto d = trash::trash_directory::construct(
                trash_dir.path().c_str(), { .create_if_not_exist = true, });

            shared_node<observer> ob;
            d->add_observer(ob);

            fs::temporary_directory file_dir;
            std::ofstream ofs { file_dir / "foo" };

            BOOST_TEST(d->empty());

            d->trash_file(file_dir / "foo");

            BOOST_REQUIRE(d->size() == 1);
            BOOST_TEST(!fs::exists(file_dir / "foo"));
            BOOST_TEST(fs::exists(trash_dir / "info" / "foo.trashinfo"));

            ob->events.clear();

            d->delete_trash(d->begin());

            BOOST_TEST(d->empty());
            BOOST_TEST(!fs::exists(file_dir / "foo"));
            BOOST_TEST(!fs::exists(trash_dir / "info" / "foo.trashinfo"));
            BOOST_TEST(!fs::exists(trash_dir / "files" / "foo"));

            auto expected_events = json::array {
                { { "event", "entry_deleted", },
                  { "directory", trash_dir.path() },
                }
            };
            BOOST_TEST(json::match(ob->events, expected_events, print_mismatch));
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(delete_trash_2_)
    {
        try {
            fs::temporary_directory trash_dir;

            install_mock_environment();
            auto d = trash::trash_directory::construct(
                trash_dir.path().c_str(), { .create_if_not_exist = true, });

            shared_node<observer> ob;
            d->add_observer(ob);

            fs::temporary_directory file_dir;
            lx::mkdir(file_dir / "foo");

            BOOST_TEST(d->empty());

            d->trash_file(file_dir / "foo");

            BOOST_REQUIRE(d->size() == 1);
            BOOST_TEST(!fs::exists(file_dir / "foo"));
            BOOST_TEST(fs::exists(trash_dir / "info" / "foo.trashinfo"));
            BOOST_TEST(d->directory_size_cache().size() == 1);

            ob->events.clear();

            d->delete_trash(d->begin());

            BOOST_TEST(d->empty());
            BOOST_TEST(!fs::exists(file_dir / "foo"));
            BOOST_TEST(!fs::exists(trash_dir / "info" / "foo.trashinfo"));
            BOOST_TEST(!fs::exists(trash_dir / "files" / "foo"));
            BOOST_TEST(d->directory_size_cache().size() == 0);

            auto expected_events = json::array {
                { { "event", "entry_deleted", },
                  { "directory", trash_dir.path() },
                }
            };
            BOOST_TEST(json::match(ob->events, expected_events, print_mismatch));
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(clear_)
    {
        try {
            fs::temporary_directory trash_dir;

            install_mock_environment();
            auto d = trash::trash_directory::construct(
                trash_dir.path().c_str(), { .create_if_not_exist = true, });

            shared_node<observer> ob;
            d->add_observer(ob);

            fs::temporary_directory file_dir;
            std::ofstream ofs1 { file_dir / "foo" };
            std::ofstream ofs2 { file_dir / "bar" };

            BOOST_TEST(d->empty());

            d->trash_file(file_dir / "foo");
            d->trash_file(file_dir / "bar");

            BOOST_REQUIRE(d->size() == 2);
            BOOST_TEST(!fs::exists(file_dir / "foo"));
            BOOST_TEST(fs::exists(trash_dir / "info" / "foo.trashinfo"));

            BOOST_TEST(!fs::exists(file_dir / "bar"));
            BOOST_TEST(fs::exists(trash_dir / "info" / "bar.trashinfo"));

            ob->events.clear();

            d->clear();

            BOOST_TEST(d->empty());
            BOOST_TEST(!fs::exists(file_dir / "foo"));
            BOOST_TEST(!fs::exists(trash_dir / "info" / "foo.trashinfo"));
            BOOST_TEST(!fs::exists(trash_dir / "files" / "foo"));

            BOOST_TEST(!fs::exists(file_dir / "bar"));
            BOOST_TEST(!fs::exists(trash_dir / "info" / "bar.trashinfo"));
            BOOST_TEST(!fs::exists(trash_dir / "files" / "bar"));

            auto expected_events = json::array {
                { { "event", "entry_deleted", },
                  { "directory", trash_dir.path() },
                },
                { { "event", "entry_deleted", },
                  { "directory", trash_dir.path() },
                },
            };
            BOOST_TEST(json::match(ob->events, expected_events, print_mismatch));
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(entry_created_)
    {
        try {
            fs::temporary_directory trash_dir;

            auto& env = install_mock_environment();
            auto d1 = trash::trash_directory::construct(
                trash_dir.path().c_str(), { .create_if_not_exist = true, });

            shared_node<observer> ob;
            d1->add_observer(ob);

            // trash file from other directory object
            fs::temporary_directory file_dir;
            std::ofstream ofs { file_dir / "fox" };

            auto d2 = trash::trash_directory::construct(trash_dir.path().c_str());
            d2->trash_file(file_dir / "fox");

            // emit notification from environment
            env.emit_file_created(d2->path() / "info", "fox.trashinfo");

            BOOST_TEST(d1->size() == 1);

            auto expected = json::array {
                json::object {
                    { "event", "entry_created" },
                    { "directory", d2->path() },
                    { "entry", d2->path() / "info" / "fox.trashinfo" },
                },
            };

            BOOST_TEST(ob->events == expected);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(entry_modified_)
    {
        try {
            fs::temporary_directory trash_dir;

            auto& env = install_mock_environment();
            auto d1 = trash::trash_directory::construct(
                trash_dir.path().c_str(), { .create_if_not_exist = true, });

            // trash file
            fs::temporary_directory file_dir;
            std::ofstream ofs { file_dir / "fox" };

            d1->trash_file(file_dir / "fox");

            shared_node<observer> ob;
            d1->add_observer(ob);

            // suppose some modification has happened at outside of the program

            // emit notification from environment
            env.emit_file_modified(d1->path() / "info", "fox.trashinfo");

            BOOST_TEST(d1->size() == 1);

#if 0
            auto expected = json::array {
                json::object {
                    { "event", "entry_modified" },
                    { "directory", d1->path() },
                    { "entry", d1->path() / "info" / "fox.trashinfo" },
                },
            };

            BOOST_TEST(ob->events == expected);
#endif
            BOOST_TEST(ob->events.empty());
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(entry_deleted_)
    {
        fs::temporary_directory trash_dir;

        auto& env = install_mock_environment();
        auto d1 = trash::trash_directory::construct(
            trash_dir.path().c_str(), { .create_if_not_exist = true, });

        fs::temporary_directory file_dir;
        std::ofstream ofs { file_dir / "cat" };

        BOOST_REQUIRE(d1->empty());
        d1->trash_file(file_dir / "cat");
        BOOST_REQUIRE(d1->size() == 1);

        shared_node<observer> ob;
        d1->add_observer(ob);

        // erase trash from separately constructed directory object
        auto d2 = trash::trash_directory::construct(trash_dir.path().c_str());

        auto it = d2->find_info_by_name("cat");
        BOOST_REQUIRE(it != d2->end());

        d2->delete_trash(it);

        // issue notification
        env.emit_file_deleted(d2->path() / "info", "cat.trashinfo");

        BOOST_TEST(d1->size() == 0);

        auto expected = json::array {
            json::object {
                { "event", "entry_deleted" },
                { "directory", d2->path() },
                { "entry", d2->path() / "info" / "cat.trashinfo" },
            },
        };

        BOOST_TEST(ob->events == expected);
    }

    BOOST_AUTO_TEST_CASE(directory_disappeared_)
    {
        try {
            fs::temporary_directory trash_dir;

            auto& env = install_mock_environment();
            auto d1 = trash::trash_directory::construct(
                trash_dir.path().c_str(), { .create_if_not_exist = true, });

            shared_node<observer> ob;
            d1->add_observer(ob);

            // issue notification
            env.emit_directory_disappeared(d1->path() / "info");

            auto expected = json::array {
                {
                    { "event", "directory_disappeared" },
                    { "directory", trash_dir.path() },
                },
            };

            BOOST_TEST(ob->events == expected);
        }
        catch (...) {
            stream9::errors::print_error();
            BOOST_TEST(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // directory_

} // namespace testing
