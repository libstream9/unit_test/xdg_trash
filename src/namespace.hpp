#ifndef STREAM9_TRASH_TEST_NAMESPACE_HPP
#define STREAM9_TRASH_TEST_NAMESPACE_HPP

namespace stream9::errors {}
namespace stream9::ranges {}
namespace stream9::json {}
namespace stream9::xdg::trash {}
namespace stream9::filesystem {}
namespace stream9::strings {}

namespace testing {

namespace st9 { using namespace stream9; }

namespace err { using namespace stream9::errors; }

namespace rng { using namespace stream9::ranges; }

namespace json { using namespace stream9::json; }

namespace trash { using namespace stream9::xdg::trash; }

namespace fs { using namespace stream9::filesystem; }

namespace str { using namespace stream9::strings; }

namespace lx { using namespace stream9::linux; }

} // namespace testing

#endif // STREAM9_TRASH_TEST_NAMESPACE_HPP
