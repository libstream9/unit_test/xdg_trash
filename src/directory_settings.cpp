#include <stream9/xdg/trash/directory_settings.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/linux_environment.hpp>
#include <stream9/xdg/trash/observer_set.hpp>

#include <boost/container/flat_set.hpp>
#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/log.hpp>
#include <stream9/log/grabber.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/weak_node.hpp>

namespace testing {

using stream9::shared_node;
using stream9::weak_node;
using trash::file_size_t;

BOOST_AUTO_TEST_SUITE(directory_settings_)

    unsigned long long operator ""_Ki (unsigned long long const v)
    {
        return v * 1024;
    }

    unsigned long long operator ""_Mi (unsigned long long const v)
    {
        return v * 1024_Ki;
    }

    unsigned long long operator ""_Gi (unsigned long long const v)
    {
        return v * 1024_Mi;
    }

    struct mock_environment : trash::linux_environment
    {
        file_size_t m_capacity = 1000;
        trash::observer_set<directory_observer> m_observers;

        mock_environment() = default;

        mock_environment(file_size_t const cap)
            : m_capacity { cap }
        {}

        void emit_file_created(std::string_view const filename)
        {
            m_observers.for_each([&](auto&& ob) {
                ob.file_created("", filename);
            });
        }

        void emit_file_modified(std::string_view const filename)
        {
            m_observers.for_each([&](auto&& ob) {
                ob.file_modified("", filename);
            });
        }

        void emit_file_deleted(std::string_view const filename)
        {
            m_observers.for_each([&](auto&& ob) {
                ob.file_deleted("", filename);
            });
        }

        // trash::environment
        file_size_t filesystem_capacity(st9::string_view) override
        {
            return m_capacity;
        }

        void add_directory_observer(st9::string_view,
                                    weak_node<directory_observer> ob) override
        {
            m_observers.insert(ob);
        }
    };

    static mock_environment&
    install_mock_environment()
    {
        shared_node<mock_environment> env;
        trash::set_env(env);
        return env;
    }

    static mock_environment&
    install_mock_environment(file_size_t cap)
    {
        shared_node<mock_environment> env { cap };
        trash::set_env(env);
        return env;
    }

    BOOST_AUTO_TEST_CASE(non_existent_directory_)
    {
        install_mock_environment();

        auto s = trash::directory_settings::construct("bogus_path");

        BOOST_TEST(s->path() == "bogus_path");
        BOOST_TEST(s->size_limit() == 1_Gi); // default value
    }

    BOOST_AUTO_TEST_CASE(directory_without_config_file_)
    {
        fs::temporary_directory dir;
        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
    }

    BOOST_AUTO_TEST_CASE(directory_with_valid_config_file_1_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        st9::ofstream ofs { p };
        ofs << "[stream9::trash]\n"
            << "SizeLimit=0.1\n"
            << std::flush;
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 100);
    }

    BOOST_AUTO_TEST_CASE(directory_with_valid_config_file_2_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        st9::ofstream ofs { p };
        ofs << "[stream9::trash]\n"
            << "SizeLimit=1\n"
            << std::flush;
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 1000);
    }

    BOOST_AUTO_TEST_CASE(directory_with_invalid_config_file_1_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        st9::ofstream ofs { p };
        ofs << "[stream9::trashx]\n" // wrong section name
            << "SizeLimit=0.1\n"
            << std::flush;
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
    }

    BOOST_AUTO_TEST_CASE(directory_with_invalid_config_file_2_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        st9::ofstream ofs { p };
        ofs << "[stream9::trash]\n"
            << "SizeLimit=\n" // empty value
            << std::flush;
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
    }

    BOOST_AUTO_TEST_CASE(directory_with_invalid_config_file_3_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        stream9::log::grabber g;

        st9::ofstream ofs { p };
        ofs << "[stream9::trash]\n"
            << "SizeLimit=1.001\n" // exceed limit
            << std::flush;
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
    }

    BOOST_AUTO_TEST_CASE(directory_with_invalid_config_file_4_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        stream9::log::grabber g;

        st9::ofstream ofs { p };
        ofs << "[stream9::trash]\n"
            << "SizeLimit=0.00\n" // exceed limit
            << std::flush;
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
    }

    BOOST_AUTO_TEST_CASE(directory_with_invalid_config_file_5_)
    {
        fs::temporary_directory dir;
        auto const p = dir / ".directory";

        stream9::log::grabber g;

        st9::ofstream ofs { p };
        ofs << "[stream9::trash]\n"
            << std::flush; // no SizeLimit key
        ofs.close();

        install_mock_environment(1000);

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->path() == dir.path());
        BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
    }

    BOOST_AUTO_TEST_SUITE(file_create_event_)

        BOOST_AUTO_TEST_CASE(valid_file_)
        {
            fs::temporary_directory dir;

            auto& env = install_mock_environment(1000);

            auto s = trash::directory_settings::construct(dir.path());

            BOOST_TEST(s->size_limit() == 10);

            st9::ofstream ofs { dir / ".directory" };
            ofs << "[stream9::trash]\n"
                << "SizeLimit=0.1\n"
                << std::flush;
            ofs.close();

            env.emit_file_created(".directory");

            BOOST_TEST(s->size_limit() == 100);
        }

        BOOST_AUTO_TEST_CASE(invalid_file_)
        {
            fs::temporary_directory dir;

            auto& env = install_mock_environment(1000);

            auto s = trash::directory_settings::construct(dir.path());

            BOOST_TEST(s->size_limit() == 10);

            st9::ofstream ofs { dir / ".directory" };
            ofs << "[stream9::trash]\n"
                << "SizeLimit=\n" // invalid
                << std::flush;
            ofs.close();

            env.emit_file_created(".directory");

            BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
        }

        BOOST_AUTO_TEST_CASE(irrelevant_file_)
        {
            fs::temporary_directory dir;

            auto& env = install_mock_environment(1000);

            st9::ofstream ofs { dir / ".directory" };
            ofs << "[stream9::trash]\n"
                << "SizeLimit=0.1\n"
                << std::flush;
            ofs.close();

            auto s = trash::directory_settings::construct(dir.path());

            BOOST_TEST(s->size_limit() == 100);

            env.emit_file_created("irrelevant_file");

            BOOST_TEST(s->size_limit() == 100); // no change
        }

    BOOST_AUTO_TEST_SUITE_END() // file_create_event_

    BOOST_AUTO_TEST_SUITE(file_modified_event_)

        BOOST_AUTO_TEST_CASE(valid_file_)
        {
            fs::temporary_directory dir;

            auto& env = install_mock_environment(1000);

            st9::ofstream ofs1 { dir / ".directory" };
            ofs1 << "[stream9::trash]\n"
                 << "SizeLimit=0.1\n"
                 << std::flush;
            ofs1.close();

            auto s = trash::directory_settings::construct(dir.path());

            BOOST_TEST(s->size_limit() == 100);

            st9::ofstream ofs2 { dir / ".directory" };
            ofs2 << "[stream9::trash]\n"
                 << "SizeLimit=0.2\n"
                 << std::flush;
            ofs2.close();

            env.emit_file_modified(".directory");

            BOOST_TEST(s->size_limit() == 200);
        }

        BOOST_AUTO_TEST_CASE(invalid_file_)
        {
            fs::temporary_directory dir;

            auto& env = install_mock_environment(1000);

            st9::ofstream ofs1 { dir / ".directory" };
            ofs1 << "[stream9::trash]\n"
                 << "SizeLimit=0.1\n"
                 << std::flush;
            ofs1.close();

            auto s = trash::directory_settings::construct(dir.path());

            BOOST_TEST(s->size_limit() == 100);

            st9::ofstream ofs2 { dir / ".directory" };
            ofs2 << "[stream9::trashx]\n" // wrong section
                 << "SizeLimit=0.2\n"
                 << std::flush;
            ofs2.close();

            env.emit_file_modified(".directory");

            BOOST_TEST(s->size_limit() == 10); // default rate 0.01 apply
        }

        BOOST_AUTO_TEST_CASE(irrelevant_file_)
        {
            fs::temporary_directory dir;

            auto& env = install_mock_environment(1000);

            st9::ofstream ofs1 { dir / ".directory" };
            ofs1 << "[stream9::trash]\n"
                 << "SizeLimit=0.1\n"
                 << std::flush;
            ofs1.close();

            auto s = trash::directory_settings::construct(dir.path());

            BOOST_TEST(s->size_limit() == 100);

            env.emit_file_modified("yadayadayada");

            BOOST_TEST(s->size_limit() == 100); // no change
        }

    BOOST_AUTO_TEST_SUITE_END() // file_modified_event_

    BOOST_AUTO_TEST_CASE(file_deleted_event_)
    {
        fs::temporary_directory dir;

        auto& env = install_mock_environment(1000);

        st9::ofstream ofs1 { dir / ".directory" };
        ofs1 << "[stream9::trash]\n"
             << "SizeLimit=0.1\n"
             << std::flush;
        ofs1.close();

        auto s = trash::directory_settings::construct(dir.path());

        BOOST_TEST(s->size_limit() == 100);

        lx::unlink(dir / ".directory");

        env.emit_file_deleted(".directory");

        BOOST_TEST(s->size_limit() == 10); // default rate apply
    }

BOOST_AUTO_TEST_SUITE_END() // directory_settings_

} // namespace testing
