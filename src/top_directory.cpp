#include <stream9/xdg/trash/top_directory.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/linux_environment.hpp>
#include <stream9/xdg/trash/observer_set.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/filesystem/remove_recursive.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/chmod.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/dirname.hpp>
#include <stream9/path/relative.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/weak_node.hpp>

namespace testing {

namespace path = st9::path;

using path::operator/;
using st9::string;
using st9::string_view;
using stream9::shared_node;
using stream9::weak_node;

static bool
print_mismatch(json::pointer const& p,
               json::value const& v1, json::value const& v2)
{
    std::cout << "mismatch at " << p
              << ", result = " << v1
              << ", expected = " << v2 << std::endl;
    return false;
};

static void
set_sticky_bit(string_view p)
{
    auto st = lx::stat(p);
    lx::chmod(p, st.st_mode | S_ISVTX);
}

BOOST_AUTO_TEST_SUITE(top_directory_)

    class mock_environment : public trash::linux_environment
    {
    public:
        mock_environment()
        {}

        // accessor
        auto& root() const { return m_root; }

        // modifier
        string create_trash_directory(string_view p_)
        {
            string p { p_ };
            if (path::is_relative(p)) {
                p = m_root.path() / p_;
            }

            fs::mkdir_recursive(p);
            lx::mkdir(p / "info");
            lx::mkdir(p / "files");

            m_observers.for_each([&](auto&& o) {
                o.file_created(path::dirname(p), p);
            });

            return p;
        }

        void delete_trash_directory(string_view p)
        {
            fs::remove_recursive(p);

            m_observers.for_each([&](auto&& o) {
                o.file_deleted(path::dirname(p), p);
            });
        }

        void add_directory_observer(st9::string_view p,
                                    weak_node<directory_observer> o) override
        {
            if (p != m_root.path()) return;

            m_observers.insert(o);
        }

    private:
        fs::temporary_directory m_root;
        trash::observer_set<environment::directory_observer> m_observers;
    };

    mock_environment& install_mock_environment()
    {
        shared_node<mock_environment> env;
        trash::set_env(env);
        return env;
    }

    class top_dir_observer : public trash::top_directory::observer
    {
    public:
        void trash_directory_added(trash::trash_directory& d) override
        {
            m_events.push_back(json::object {
                { "type", "trash_directory_added" },
                { "path", d.path() },
            });
        }

        void trash_directory_about_to_be_removed(trash::trash_directory& d) override
        {
            m_events.push_back(json::object {
                { "type", "trash_directory_about_to_be_removed" },
                { "path", d.path() },
            });
        }

        auto& events() const noexcept { return m_events; }

    private:
        json::array m_events;
    };

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto& env = install_mock_environment();
            auto top = trash::top_directory::construct(env.root().path().c_str());


            BOOST_TEST(top->empty());
        }

        BOOST_AUTO_TEST_CASE(has_trash_dirs_)
        {
            auto& env = install_mock_environment();
            env.create_trash_directory("wrong_name");
            auto p1 = env.create_trash_directory(".Trash-1000");
            env.create_trash_directory(".Trash-wrong_uid");

            fs::mkdir_recursive(env.root() / ".Trash");
            set_sticky_bit(env.root() / ".Trash");

            env.create_trash_directory(".Trash/wrong_uid");
            auto p2 = env.create_trash_directory(".Trash/1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            json::object expected {
                { "trash_dirs", json::array {
                                    { { "path", p2 } }, { { "path", p1 } },
                                } },
            };

            auto result = json::value_from(*top);

            BOOST_TEST(json::match(result, expected, print_mismatch));
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(primary_trash_directory_)

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto& env = install_mock_environment();

            auto top = trash::top_directory::construct(env.root().path().c_str());

            auto* d = top->primary_trash_directory();

            BOOST_REQUIRE(!d);
        }

        BOOST_AUTO_TEST_CASE(has_type1_)
        {
            auto& env = install_mock_environment();

            fs::mkdir_recursive(env.root() / ".Trash");
            set_sticky_bit(env.root() / ".Trash");

            env.create_trash_directory(".Trash/wrong_uid");
            auto p1 = env.create_trash_directory(".Trash/1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            auto* d = top->primary_trash_directory();

            BOOST_REQUIRE(d);
            BOOST_TEST(d->path() == p1);
        }

        BOOST_AUTO_TEST_CASE(has_type2_)
        {
            auto& env = install_mock_environment();
            auto p1 = env.create_trash_directory(".Trash-1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            auto* d = top->primary_trash_directory();

            BOOST_REQUIRE(d);
            BOOST_TEST(d->path() == p1);
        }

        BOOST_AUTO_TEST_CASE(has_both_)
        {
            auto& env = install_mock_environment();
            auto p1 = env.create_trash_directory(".Trash-1000");

            fs::mkdir_recursive(env.root() / ".Trash");
            set_sticky_bit(env.root() / ".Trash");

            env.create_trash_directory(".Trash/wrong_uid");
            auto p2 = env.create_trash_directory(".Trash/1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            auto* d = top->primary_trash_directory();

            BOOST_REQUIRE(d);
            BOOST_TEST(d->path() == p2);
        }

    BOOST_AUTO_TEST_SUITE_END() // primary_trash_directory_

    BOOST_AUTO_TEST_SUITE(find_or_create_trash_directory_)

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto& env = install_mock_environment();
            auto top = trash::top_directory::construct(env.root().path().c_str());

            shared_node<top_dir_observer> ob;
            top->add_observer(ob);

            auto* d = top->find_or_create_trash_directory();
            BOOST_REQUIRE(d);

            BOOST_TEST(top->size() == 1);

            auto& ev = ob->events();

            json::array expected { json::object {
                { "type", "trash_directory_added" },
                { "path", env.root() / ".Trash-1000" },
            } };

            BOOST_TEST(ev == expected);
        }

        BOOST_AUTO_TEST_CASE(has_type1_)
        {
            auto& env = install_mock_environment();

            fs::mkdir_recursive(env.root() / ".Trash");
            set_sticky_bit(env.root() / ".Trash");

            env.create_trash_directory(".Trash/wrong_uid");
            auto p1 = env.create_trash_directory(".Trash/1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            shared_node<top_dir_observer> ob;
            top->add_observer(ob);

            auto* d = top->find_or_create_trash_directory();

            BOOST_REQUIRE(d);
            BOOST_TEST(d->path() == p1);
            BOOST_TEST(ob->events().empty());
        }

        BOOST_AUTO_TEST_CASE(has_type2_)
        {
            auto& env = install_mock_environment();
            auto p1 = env.create_trash_directory(".Trash-1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            auto* d = top->find_or_create_trash_directory();

            shared_node<top_dir_observer> ob;
            top->add_observer(ob);

            BOOST_REQUIRE(d);
            BOOST_TEST(d->path() == p1);
            BOOST_TEST(ob->events().empty());
        }

        BOOST_AUTO_TEST_CASE(has_both_)
        {
            auto& env = install_mock_environment();
            auto p1 = env.create_trash_directory(".Trash-1000");

            fs::mkdir_recursive(env.root() / ".Trash");
            set_sticky_bit(env.root() / ".Trash");

            env.create_trash_directory(".Trash/wrong_uid");
            auto p2 = env.create_trash_directory(".Trash/1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());

            shared_node<top_dir_observer> ob;
            top->add_observer(ob);

            auto* d = top->find_or_create_trash_directory();

            BOOST_REQUIRE(d);
            BOOST_TEST(d->path() == p2);
            BOOST_TEST(ob->events().empty());
        }

    BOOST_AUTO_TEST_SUITE_END() // find_or_create_trash_directory_

    BOOST_AUTO_TEST_SUITE(directory_event_)

        BOOST_AUTO_TEST_CASE(file_created_)
        {
            auto& env = install_mock_environment();

            auto top = trash::top_directory::construct(env.root().path().c_str());

            shared_node<top_dir_observer> ob;
            top->add_observer(ob);

            // create trash directory from outside after top_directory is initialized
            auto p1 = env.create_trash_directory(".Trash-1000");

            auto result = json::value_from(*top);

            json::object expected1 {
                { "trash_dirs", json::array {
                    { { "path", p1 } } }}
            };

            // new directory gets scanned as expected
            BOOST_TEST(json::match(result, expected1, print_mismatch));

            json::array expected2 { json::object {
                { "type", "trash_directory_added" },
                { "path", p1 },
            }};

            BOOST_TEST(ob->events() == expected2);
        }

        BOOST_AUTO_TEST_CASE(file_deleted_)
        {
            auto& env = install_mock_environment();

            auto p1 = env.create_trash_directory(".Trash-1000");

            auto top = trash::top_directory::construct(env.root().path().c_str());
            BOOST_REQUIRE(top->size() == 1);

            shared_node<top_dir_observer> ob;
            top->add_observer(ob);

            env.delete_trash_directory(p1);

            BOOST_TEST(top->empty());

            json::array expected { json::object {
                { "type", "trash_directory_about_to_be_removed" },
                { "path", p1 },
            }};

            BOOST_TEST(ob->events() == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // directory_events_

BOOST_AUTO_TEST_SUITE_END() // top_directory_

} // namespace testing
